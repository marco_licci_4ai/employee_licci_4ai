package com.marcolicci;

public class Main {

    public static void main(String[] args) {
        Employee employee = new Employee("Mario", "Rossi", "MARIOROSSI00");
        SalariedEmployee salariedEmployee = new SalariedEmployee("Mario", "Rossi", "MARIOROSSI00", 2000);
        HourlyEmployee hourlyEmployee = new HourlyEmployee("Mario", "Rossi", "MARIOROSSI00", 50);
        CommissionEmployee commissionEmployee = new CommissionEmployee("Mario", "Rossi", "MARIOROSSI00", 15, 0);
        BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee("Mario", "Rossi", "MARIOROSSI00", 15, 0, 1000);

        System.out.println("\nClass: " + "Employee" +
                "\nFirst name: " + employee.getFirstName() +
                "\nLast name: " + employee.getLastName() +
                "\nFiscal code: " + employee.getFiscalCode()
        );

        System.out.println("\nClass: " + "SalariedEmployee" +
                "\nFirst name: " + salariedEmployee.getFirstName() +
                "\nLast name: " + salariedEmployee.getLastName() +
                "\nFiscal code: " + salariedEmployee.getFiscalCode() +
                "\nWeekly salary: " + salariedEmployee.getWeeklySalary()
        );

        System.out.println("\nClass: " + "HourlyEmployee" +
                "\nFirst name: " + hourlyEmployee.getFirstName() +
                "\nLast name: " + hourlyEmployee.getLastName() +
                "\nFiscal code: " + hourlyEmployee.getFiscalCode() +
                "\nHourly salary: " + hourlyEmployee.getHourlySalary()
        );

        System.out.println("\nClass: " + "CommissionEmployee" +
                "\nFirst name: " + commissionEmployee.getFirstName() +
                "\nLast name: " + commissionEmployee.getLastName() +
                "\nFiscal code: " + commissionEmployee.getFiscalCode() +
                "\nCommission rate: " + commissionEmployee.getCommissionRate() +
                "\nSales: " + commissionEmployee.getSales()
        );

        System.out.println("\nClass: " + "CommissionEmployee" +
                "\nFirst name: " + basePlusCommissionEmployee.getFirstName() +
                "\nLast name: " + basePlusCommissionEmployee.getLastName() +
                "\nFiscal code: " + basePlusCommissionEmployee.getFiscalCode() +
                "\nCommission rate: " + basePlusCommissionEmployee.getCommissionRate() +
                "\nSales: " + basePlusCommissionEmployee.getSales() +
                "\nBase salary: " + basePlusCommissionEmployee.getSales()
        );
    }
}
