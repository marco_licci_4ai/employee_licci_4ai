package com.marcolicci;

/**
 * Created by marco on 02/12/2016.
 */
public class CommissionEmployee extends Employee {
    private int commissionRate;
    private int sales;

    public CommissionEmployee(String firstName, String lastName, String fiscalCode, int commissionRate, int sales) {
        super(firstName, lastName, fiscalCode);
        this.commissionRate = commissionRate;
        this.sales = sales;
    }

    public int getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(int commissionRate) {
        this.commissionRate = commissionRate;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }
}
