package com.marcolicci;

/**
 * Created by marco on 02/12/2016.
 */
public class BasePlusCommissionEmployee extends CommissionEmployee {
    private int baseSalary;

    public BasePlusCommissionEmployee(String firstName, String lastName, String fiscalCode, int commissionRate, int sales, int baseSalary) {
        super(firstName, lastName, fiscalCode, commissionRate, sales);
        this.baseSalary = baseSalary;
    }

    public int getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(int baseSalary) {
        this.baseSalary = baseSalary;
    }
}
