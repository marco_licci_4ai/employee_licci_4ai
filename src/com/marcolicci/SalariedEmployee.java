package com.marcolicci;

/**
 * Created by marco on 02/12/2016.
 */
public class SalariedEmployee extends Employee {
    private int weeklySalary;

    public SalariedEmployee(String firstName, String lastName, String fiscalCode, int weeklySalary) {
        super(firstName, lastName, fiscalCode);
        this.weeklySalary = weeklySalary;
    }

    public int getWeeklySalary() {
        return weeklySalary;
    }

    public void setWeeklySalary(int weeklySalary) {
        this.weeklySalary = weeklySalary;
    }
}
