package com.marcolicci;

/**
 * Created by marco on 02/12/2016.
 */
public class HourlyEmployee extends Employee {
    private int hourlySalary;

    public HourlyEmployee(String firstName, String lastName, String fiscalCode, int hourlySalary) {
        super(firstName, lastName, fiscalCode);
        this.hourlySalary = hourlySalary;
    }

    public int getHourlySalary() {

        return hourlySalary;
    }

    public void setHourlySalary(int hourlySalary) {
        this.hourlySalary = hourlySalary;
    }
}
